Source: pytest-bdd
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Brian May <bam@debian.org>
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 pybuild-plugin-pyproject,
 python3-all,
 python3-execnet,
 python3-glob2,
 python3-mako,
 python3-parse,
 python3-parse-type,
 python3-poetry,
 python3-pytest,
 python3-typing-extensions,
 python3-setuptools
Standards-Version: 4.5.0
Homepage: https://github.com/pytest-dev/pytest-bdd/
Vcs-Git: https://salsa.debian.org/python-team/packages/pytest-bdd.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/pytest-bdd
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: python3-pytest-bdd
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: BDD plugin for py.test
 Implements a subset of Gherkin language for the automation of the
 project requirements testing and easier behavioral driven development.
 .
 Unlike many other BDD tools it doesn't require a separate runner and benefits
 from the power and flexibility of the pytest. It allows one to unify your unit
 and functional tests, easier continuous integration server configuration and
 maximal reuse of the tests setup.
 .
 Pytest fixtures written for the unit tests can be reused for the setup and
 actions mentioned in the feature steps with dependency injection, which allows
 a true BDD just-enough specification of the requirements without maintaining
 any context object containing the side effects of the Gherkin imperative
 declarations.
 .
 This package contains the Python 3 version.
